import 'dart:io';

class Taobin {
  String? order;
  int price = 0;
  int category = 0;
  int menu = 0;
  String? levelSweet;
  int cash = 0;
  int change = 0;

  Taobin() {
    this.order = order;
    this.price = price;
    this.category = category;
    this.menu = menu;
    this.levelSweet = levelSweet;
  }
  process() {
    showCategory();
    showMenu();
    selectMenu();
    sweetLevel();
    showOrder();
    payment();
  }

  showCategory() {
    print("\nเลือกหมวดหมู่เมนูที่ต้องการ");
    print("1. กาแฟ");
    print("2. ชา");
    print("3. นม โกโก้ และคาราเมล");
    print("4. โปรตีนเชค");
    print("5. น้ำโซดา และอื่น ๆ");
    print("6. เมนูแนะนำ");
  }

  showMenu() {
    category = int.parse(stdin.readLineSync()!);
    switch (category) {
      case (1):
        print("\nเลือกเมนูกาแฟที่ต้องการ :");
        print("1. อเมริกาโน่เย็น 60 บาท");
        print("2. ลาเต้เย็น 40 บาท");
        print("3. คาปูชิโน่เย็น 45 บาท");
        print("4. มอคค่าเย็น 40 บาท");
        print("5. กาแฟชาไทยเย็น 45 บาท");
        break;
      case (2):
        print("\nเลือกเมนูชาที่ต้องการ :");
        print("1. ชาไทยร้อน 35 บาท");
        print("2. ชาขิงร้อน 20 บาท");
        print("3. ชานมไต้หวันร้อน 35 บาท");
        print("4. ชามะนาวร้อน 20 บาท");
        print("5. ชาเขียวณี่ปุ่นร้อน 20 บาท");
        break;
      case (3):
        print("\nเลือกเมนูนม โกโก้ และคาราเมลที่ต้องการ :");
        print("1. นมคาราเมลร้อน 35 บาท");
        print("2. โกโก้ร้อน 35 บาท");
        print("3. นมร้อน 30 บาท");
        print("4. นมสดสตอเบอร์รี่ปั่น 45 บาท");
        print("5. โอริโอ้ปั่นภูเขาไฟ 55 บาท");
        break;
      case (4):
        print("\nเลือกเมนูโปรตีนเชคที่ต้องการ :");
        print("1. มัทฉะโปรตีน 60 บาท");
        print("2. โกโก้โปรตีน 60 บาท");
        print("3. ชานมไทยโปรตีน 60 บาท");
        print("4. ชาไต้หวันโปรตีน 60 บาท");
        print("5. คาราเมลโปรตีน 60 บาท");
        break;
      case (5):
        print("\nเลือกเมนูน้ำโซดา และอื่น ๆ ที่ต้องการ :");
        print("1. น้ำมะนาวโซดา 20 บาท");
        print("2. น้ำลิ้นจี่โซดา 25 บาท");
        print("3. น้ำแดงโซดา 20 บาท");
        print("4. กัญชาโซดา 40 บาท");
        print("5. ขิงโซดา 25 บาท");
        break;
      case (6):
        print("\nเลือกเมนูแนะนำที่ต้องการ :");
        print("1. นมสตอเบอร์รี่ปั่น 45 บาท");
        print("2. โกโก้สตอเบอร์รี่ปั่น 45 บาท");
        print("3. โอริโอ้ปั่นภูเขาไฟ 55 บาท");
        print("4. โกโก้เย็นปั่น 40 บาท");
        print("5. กัญชาโซดา 40 บาท");
        break;
    }
  }

  selectMenu() {
    menu = int.parse(stdin.readLineSync()!);
    switch (category) {
      case (1):
        coffee(menu);
        break;
      case (2):
        tea(menu);
        break;
      case (3):
        milk(menu);
        break;
      case (4):
        protein(menu);
        break;
      case (5):
        soda(menu);
        break;
      case (6):
        recoment(menu);
        break;
    }
  }

  coffee(int menu) {
    switch (menu) {
      case (1):
        order = "อเมริกาโน่เย็น";
        price = 60;
        break;
      case (2):
        order = "ลาเต้เย็น";
        price = 40;
        break;
      case (3):
        order = "คาปูชิโน่เย็น";
        price = 45;
        break;
      case (4):
        order = "มอคค่าเย็น";
        price = 40;
        break;
      case (5):
        order = "กาแฟชาไทยเย็น";
        price = 45;
        break;
    }
  }

  tea(int menu) {
    switch (menu) {
      case (1):
        order = "ชาไทยร้อน";
        price = 35;
        break;
      case (2):
        order = "ชาขิงร้อน";
        price = 20;
        break;
      case (3):
        order = "ชานมไต้หวันร้อน";
        price = 35;
        break;
      case (4):
        order = "ชามะนาวร้อน";
        price = 20;
        break;
      case (5):
        order = "ชาเขียวณี่ปุ่นร้อน";
        price = 20;
        break;
    }
  }

  milk(int menu) {
    switch (menu) {
      case (1):
        order = "นมคาราเมลร้อน";
        price = 35;
        break;
      case (2):
        order = "โกโก้ร้อน";
        price = 35;
        break;
      case (3):
        order = "นมร้อน";
        price = 30;
        break;
      case (4):
        order = "นมสดสตอเบอร์รี่ปั่น";
        price = 45;
        break;
      case (5):
        order = "โอริโอ้ปั่นภูเขาไฟ";
        price = 55;
        break;
    }
  }

  protein(int menu) {
    switch (menu) {
      case (1):
        order = "มัทฉะโปรตีน";
        price = 60;
        break;
      case (2):
        order = "โกโก้โปรตีน";
        price = 60;
        break;
      case (3):
        order = "ชานมไทยโปรตีน";
        price = 60;
        break;
      case (4):
        order = "ชาไต้หวันโปรตีน";
        price = 60;
        break;
      case (5):
        order = "คาราเมลโปรตีน";
        price = 60;
        break;
    }
  }

  soda(int menu) {
    switch (menu) {
      case (1):
        order = "น้ำมะนาวโซดา";
        price = 20;
        break;
      case (2):
        order = "น้ำลิ้นจี่โซดา";
        price = 25;
        break;
      case (3):
        order = "น้ำแดงโซดา";
        price = 20;
        break;
      case (4):
        order = "กัญชาโซดา";
        price = 40;
        break;
      case (5):
        order = "ขิงโซดา";
        price = 25;
        break;
    }
  }

  recoment(int menu) {
    switch (menu) {
      case (1):
        order = "นมสตอเบอร์รี่ปั่น";
        price = 45;
        break;
      case (2):
        order = "โกโก้สตอเบอร์รี่ปั่น";
        price = 45;
        break;
      case (3):
        order = "โอริโอ้ปั่นภูเขาไฟ";
        price = 55;
        break;
      case (4):
        order = "โกโก้เย็นปั่น";
        price = 40;
        break;
      case (5):
        order = "กัญชาโซดา";
        price = 40;
        break;
    }
  }

  sweetLevel() {
    print("\nเลือกระดับความหวาน :");
    print("0. ไม่ใส่น้ำตาล");
    print("1. หวานน้อย");
    print("2. หวานพอดี");
    print("3. หวานมาก");
    print("4. หวาน 3 โลก");
    int level = int.parse(stdin.readLineSync()!);
    switch (level) {
      case 0:
        levelSweet = "ไม่ใส่น้ำตาล";
        break;
      case 1:
        levelSweet = "หวานน้อย";
        break;
      case 2:
        levelSweet = "หวานพอดี";
        break;
      case 3:
        levelSweet = "หวานมาก";
        break;
      case 4:
        levelSweet = "หวาน 3 โลก";
        break;
      default:
    }
  }

  payment() {
    print("\nกรอกจำนวนเงินที่ชำระ :");
    do {
      cash = int.parse(stdin.readLineSync()!);
      print("\nเงินไม่พอ กรุณาชำระใหม่ :");
    } while (cash<price);
      print("\nรับเงินมา : " + cash.toString() + " บาท");
      print("ราคาสินค้า : " + price.toString() + " บาท");
      change = cash - price;
      print("เงินทอน : " + change.toString() + " บาท");
    
  }

  showOrder() {
    print("\nรายการสั่งซื้อ :");
    print(order.toString() +
        " " +
        levelSweet.toString() +
        " " +
        price.toString() +
        " บาท");
  }
}
