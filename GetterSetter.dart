class Student {
  String sName = ' null ';

  String get getName {
    return sName;
  }

  set setName(String name) {
    sName = name;
  }
}

void main() {
  Student s1 = Student();
  s1.setName = " This is the tutorial for classes ! ";
  print(" Hey, ${s1.getName} ");
}
